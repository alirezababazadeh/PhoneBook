package com.asredanesh.PhoneBook.controller;

import com.asredanesh.PhoneBook.dto.ContactDto;
import com.asredanesh.PhoneBook.service.ContactService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/contacts")
public class ContactController {

    public final ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @PutMapping
    public ContactDto addContact(@RequestBody ContactDto contactDto) {
        return this.contactService.add(contactDto);
    }

    @PostMapping("/search")
    public List<ContactDto> findContacts(@RequestBody ContactDto contactDto) {
        return this.contactService.find(contactDto);
    }
}
