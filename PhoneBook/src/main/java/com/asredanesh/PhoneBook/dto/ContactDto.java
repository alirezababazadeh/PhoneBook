package com.asredanesh.PhoneBook.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ContactDto {

    private String name;
    private String phoneNumber;
    private String email;
    private String organization;
    private String github;
}
