package com.asredanesh.PhoneBook.service;


import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestService {

    private final RestTemplate restTemplate;

    public RestService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getContactGithubRepos(String githubName) {
        String url = "https://api.github.com/users/" + githubName + "/repos";
        return restTemplate.getForObject(url, String.class);
    }
}
