package com.asredanesh.PhoneBook.service;

import com.asredanesh.PhoneBook.model.Contact;
import com.asredanesh.PhoneBook.dto.ContactDto;
import com.asredanesh.PhoneBook.model.Repository;
import com.asredanesh.PhoneBook.repository.ContactRepository;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class ContactService {

    private final ContactRepository contactRepository;
    private final RestService restService;

    public ContactService(ContactRepository contactRepository, RestService restService) {
        this.contactRepository = contactRepository;
        this.restService = restService;
    }

    public ContactDto add(ContactDto contactDto) {
        return contactToContactDto(contactRepository.save(contactDtoToContact(contactDto)));
    }

    public List<ContactDto> find(ContactDto contactDto) {
        Contact contact = contactDtoToContact(contactDto);
        return contactRepository.findContacts(
                contact.getName(),
                contact.getPhoneNumber(),
                contact.getEmail(),
                contact.getOrganization(),
                contact.getGithub()
        ).stream().map(this::contactToContactDto).collect(Collectors.toList());
    }

    private Contact contactDtoToContact(ContactDto contactDto) {
        Contact contact = new Contact();
        contact.setName(contactDto.getName());
        contact.setPhoneNumber(contactDto.getPhoneNumber());
        contact.setEmail(contactDto.getEmail());
        contact.setOrganization(contactDto.getOrganization());
        contact.setGithub(contactDto.getGithub());
        contact.setRepos(findRepos(contactDto.getGithub()));
        return contact;
    }

    private ContactDto contactToContactDto(Contact contact) {
        ContactDto contactDto = new ContactDto();
        contactDto.setName(contact.getName());
        contactDto.setPhoneNumber(contact.getPhoneNumber());
        contactDto.setEmail(contact.getEmail());
        contactDto.setOrganization(contact.getOrganization());
        contactDto.setGithub(contact.getGithub());
        return contactDto;
    }

    @SneakyThrows
    private List<Repository> findRepos(String github) {
        JSONArray reposResponse = new JSONArray(restService.getContactGithubRepos(github));
        if (reposResponse.length() != 0) {
            List<JSONObject> repos = new ArrayList<>();
            for (int i = 0; i < reposResponse.length(); i++) {
                repos.add(reposResponse.getJSONObject(i));
            }
            return repos.stream().map(repo -> {
                try {
                    return new Repository(repo.getString("full_name"));
                } catch (JSONException e) {
                    return null;
                }
            }).collect(Collectors.toList());
        } else
            return Collections.emptyList();
    }
}
