package com.asredanesh.PhoneBook.repository;

import com.asredanesh.PhoneBook.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.Email;
import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

    @Query("FROM Contact c WHERE " +
            "(:name is null or c.name = :name) and " +
            "(:phoneNumber is null or c.phoneNumber = :phoneNumber) and " +
            "(:email is null or c.email = :email) and " +
            "(:organization is null or c.organization = :organization) and " +
            "(:github is null or c.github = :github)")
    List<Contact> findContacts(
            String name,
            String phoneNumber,
            @Email(message = "Email should be valid") String email,
            String organization,
            String github
    );
}
