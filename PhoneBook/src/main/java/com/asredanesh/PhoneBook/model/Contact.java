package com.asredanesh.PhoneBook.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "CONTACT")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    @Size(max = 100)
    private String name;

    @Column(name = "PHONE_NUMBER", nullable = false)
    @Pattern(regexp="(^$|[0-9]{10})")
    private String phoneNumber;

    @Column(name = "EMAIL", nullable = false)
    @Email(message = "Email should be valid")
    private String email;

    @Column(name = "ORGANIZATION", nullable = false)
    @Size(max = 100)
    private String organization;

    @Column(name = "GITHUB", nullable = false)
    private String github;

    @OneToMany(targetEntity = Repository.class, cascade = CascadeType.ALL)
    private List<Repository> repos;

}
